import { Test, TestingModule } from '@nestjs/testing';
import { RoleService } from './role.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { User } from '../entity/user.entity';
import { Role } from '../entity/role.entity';

describe('RoleService', () => {
  let service: RoleService;
  const roleRepository = {
    save: jest.fn().mockImplementation(dto => Promise.resolve({ id: Date.now(), ...dto})),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RoleService, {
        provide: getRepositoryToken(Role),
        useValue: roleRepository,
      }],

    }).compile();

    service = module.get<RoleService>(RoleService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create new  a role', async () => {
    expect( await service.createRole({ description: 'рандомный роль 1', value: 'рандом 1' })).toEqual({
      id:expect.any(Number),
      description:'рандомный роль 1',
      value:'рандом 1',
      status:201,
      error:null
    });
  });
});