import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from '../entity/role.entity';
import { Repository } from 'typeorm';
import { CreateRoleDto } from '../user.dto';
import { CreateRoleResponse, GetRolesResponse } from '../user.pb';

@Injectable()
export class RoleService {
  constructor(@InjectRepository(Role) private roleRepository: Repository<Role>) {
  }

  async createRole(dto: CreateRoleDto): Promise<CreateRoleResponse> {
    // const candidate = await this.getRoleByValue(dto.value);
    // if (candidate) {
    //   return { error: ['такой роль уже существует'], id: 0, status: 422 };
    // }
    const role = await this.roleRepository.save({ value: dto.value, description: dto.description });
    return { id: role.id, status: HttpStatus.CREATED, error: null };
  }

  async getRoleByValue(value: string) {
    return this.roleRepository.findOne({ where: { value } });
  }

  async getRoles(): Promise<GetRolesResponse> {
    const roles = await this.roleRepository.find();
    return { roles, error: null, status: HttpStatus.OK };
  }

}