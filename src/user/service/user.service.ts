import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserResponse, Empty, GetUserByIdResponse, LoginUserResponse } from '../user.pb';
import { User } from '../entity/user.entity';
import { AddRoleAndRemoveUser, CreateUserDto, GetOneUser, LoginRequest } from '../user.dto';
import { RoleService } from './role.service';

@Injectable()
export class UserService {
  constructor(@InjectRepository(User) private userRepository: Repository<User>, private roleService: RoleService) {
  }

  async createUser(dto: CreateUserDto): Promise<CreateUserResponse> {
    try {
      const candidate = await this.getUserByPhone(dto.phone);
      console.log("test");
      if (candidate) {
        return { id: 0, error: ['такой пользователь с таким телефон номером уже есть'], status: 422 };
      }
      const user = await this.userRepository.save({ phone: dto.phone, password: dto.password });
      let role :any = await this.roleService.getRoleByValue("USER")
      if (!role) {
        role = await this.roleService.createRole({description: `USER сайта`, value: "USER"});
      }
      user.roles = [role];
      await this.userRepository.save(user);
      return { status: 201, error: null, id: user.id };
    } catch (e) {
      return { status: 500, error: [e], id: 0 };
    }
  }

  async getUserByPhone(phone: string) {
    return await this.userRepository.findOne({ where: { phone } });
  }

  async getUser(dto: LoginRequest): Promise<LoginUserResponse> {
    const user = await this.getUserByPhone(dto.phone);
    if (!user) {
      return { error: ['такого пользователя нету'], status: 404, id: 0, password: '', phone: '' };
    }
    return { status: HttpStatus.OK, error: null, id: user.id, password: user.password, phone: user.phone };
  }

  async addRoleToUser(dto: AddRoleAndRemoveUser): Promise<Empty> {
    const { user, role } = await this.workWithRole(dto);
    if (user == null || role == null) {
      return { error: ['пользовтель или роль не найдена'], status: HttpStatus.BAD_REQUEST };
    }
    user.roles = [...user.roles, role];
    await this.userRepository.save(user);
    return { status: HttpStatus.OK, error: null };
  }

  async removeRoleToUser(dto: AddRoleAndRemoveUser): Promise<Empty> {
    const { user, role } = await this.workWithRole(dto);
    if (user == null || role == null) {
      return { error: ['пользовтель или роль не найдена'], status: HttpStatus.BAD_REQUEST };
    }
    const roles = user.roles.filter(e => e.value !== role.value);
    user.roles = [...roles];
    await this.userRepository.save(user);
    return { status: HttpStatus.OK, error: null };
  }

  async getUserById(id: number) {
    return await this.userRepository.findOne({ where: { id:+id }, relations: ['roles'] });
  }

  async workWithRole(dto: AddRoleAndRemoveUser) {
    const user = await this.getUserById(dto.userId);
    const role = await this.roleService.getRoleByValue(dto.role);
    if (!user || !role) {
      return { user: null, role: null };
    }
    return { user, role };
  }

  async getOneUser(dto: GetOneUser): Promise<GetUserByIdResponse> {
    const user = await this.getUserById(+ dto.userId);
    if (!user) {
      return { error: ['такой пользователь не найден'], roles: null, status: HttpStatus.NOT_FOUND, userId: 0 };
    }
    return { userId: user.id, roles: user.roles, status: HttpStatus.OK, error: null };
  }

  async login(dto: CreateUserDto): Promise<CreateUserResponse> {
    const user = await this.getUserByPhone(dto.phone);
    if (!user) {
      return { error: ['такой пользователь не найден'], status: HttpStatus.NOT_FOUND, id: 0 };
    }
    return { error: null, status: HttpStatus.OK, id: user.id };
  }
}
