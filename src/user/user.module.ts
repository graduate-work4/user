import { Module } from '@nestjs/common';
import { UserController } from './controller/user.controller';
import { UserService } from './service/user.service';
import { RoleController } from './controller/role.controller';
import { RoleService } from './service/role.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from './entity/role.entity';
import { User } from './entity/user.entity';

@Module({
  controllers: [UserController,RoleController],
  providers: [UserService,RoleService],
  imports:[TypeOrmModule.forFeature([Role,User])]
})
export class UserModule {}
