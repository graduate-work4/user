import {
  CreateRoleRequest,
  CreateRoleResponse,
  GetRolesRequest,
  GetRolesResponse, ROLE_SERVICE_NAME,
  RoleServiceController,
} from '../user.pb';
import { Observable } from 'rxjs';
import { RoleService } from '../service/role.service';
import { GrpcMethod } from '@nestjs/microservices';
import { Controller } from '@nestjs/common';
@Controller()
export class RoleController implements RoleServiceController {
  constructor(private roleService: RoleService) {
  }
  @GrpcMethod(ROLE_SERVICE_NAME, 'createRole')
  createRole(request: CreateRoleRequest): Promise<CreateRoleResponse> | Observable<CreateRoleResponse> | CreateRoleResponse {
    return this.roleService.createRole(request);
  }
  @GrpcMethod(ROLE_SERVICE_NAME, 'getRoles')
  getRoles(request: GetRolesRequest): Promise<GetRolesResponse> | Observable<GetRolesResponse> | GetRolesResponse {
    return this.roleService.getRoles();
  }

}
