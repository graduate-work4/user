import { Controller } from '@nestjs/common';
import {
  AddRoleAndRemoveRequest,
  CreateUserRequest,
  CreateUserResponse,
  Empty,
  GetUserByIdRequest,
  GetUserByIdResponse,
  GetUserForCheck,
  LoginUserResponse,
  ROLE_SERVICE_NAME,
  USER_SERVICE_NAME,
  UserServiceController,
} from '../user.pb';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';
import { GrpcMethod } from '@nestjs/microservices';

@Controller()
export class UserController implements UserServiceController {
  constructor(private userService: UserService) {
  }
  @GrpcMethod(USER_SERVICE_NAME, 'addRoleToUser')
  addRoleToUser(request: AddRoleAndRemoveRequest): Promise<Empty> | Observable<Empty> | Empty {
    return this.userService.addRoleToUser(request);
  }
  @GrpcMethod(USER_SERVICE_NAME, 'createUser')
  createUser(request: CreateUserRequest): Promise<CreateUserResponse> | Observable<CreateUserResponse> | CreateUserResponse {
    return this.userService.createUser(request);
  }
  @GrpcMethod(USER_SERVICE_NAME, 'getUser')
  getUser(request: CreateUserRequest): Promise<CreateUserResponse> | Observable<CreateUserResponse> | CreateUserResponse {
    return this.userService.login(request);
  }
  @GrpcMethod(USER_SERVICE_NAME, 'getUserById')
  getUserById(request: GetUserByIdRequest): Promise<GetUserByIdResponse> | Observable<GetUserByIdResponse> | GetUserByIdResponse {
    return this.userService.getOneUser(request);
  }
  @GrpcMethod(USER_SERVICE_NAME, 'getUserForLogin')
  getUserForLogin(request: GetUserForCheck): Promise<LoginUserResponse> | Observable<LoginUserResponse> | LoginUserResponse {
    return this.userService.getUser(request);
  }
  @GrpcMethod(USER_SERVICE_NAME, 'removeRoleToUser')
  removeRoleToUser(request: AddRoleAndRemoveRequest): Promise<Empty> | Observable<Empty> | Empty {
    return this.userService.removeRoleToUser(request);
  }
}
