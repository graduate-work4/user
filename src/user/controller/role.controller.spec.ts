import { RoleController } from './role.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { RoleService } from '../service/role.service';

describe('RoleController', () => {
  let controller: RoleController;
  const roleService = {
    create:jest.fn(dto=>{
      return {id:Date.now()}
    })
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RoleController],
      providers: [RoleService],
    }).overrideProvider(RoleService).useValue(roleService).compile();

    controller = module.get<RoleController>(RoleController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create new  a role',()=>{
    expect(controller.createRole({description:'рандомный роль',value:'рандом'}))
  })
});