import { BaseEntity, Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Role } from './role.entity';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  phone: string;
  @Column()
  password: string;
  @ManyToMany(() => Role, role => role.users)
  @JoinTable()
  roles: Role [];
}