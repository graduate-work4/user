import {
  AddRoleAndRemoveRequest,
  CreateRoleRequest,
  CreateUserRequest,
  GetUserByIdRequest,
  GetUserForCheck,
} from './user.pb';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateUserDto implements CreateUserRequest {
  @IsNotEmpty({ message: 'пороль не может быть пустым' })
  password: string;
  @IsNotEmpty({ message: 'телефон номер не может быть пустым' })
  phone: string;
}

export class CreateRoleDto implements CreateRoleRequest {
  @IsNotEmpty({message:'описание роли не должно быть пустым'})
  description: string;
  @IsNotEmpty({message:'навзвание роли не должно быть пустым'})
  value: string;

}

export class AddRoleAndRemoveUser implements AddRoleAndRemoveRequest {
  @IsNotEmpty({message:'описание роли не должно быть пустым'})
  @IsString({message:'название роли должна быть стракой'})
  role: string;
  @IsNotEmpty({message:'описание роли не должно быть пустым'})
  userId: number;
}

export class LoginRequest implements GetUserForCheck {
  @IsNotEmpty({message:'телефон номер не должно быть пустым'})
  @IsString({message:'номер должна быть стракой'})
  phone: string;
}

export class GetOneUser implements GetUserByIdRequest {
  error: string[];
  status: number;
  @IsNotEmpty({message:'id пользователя не должно быть пустым'})
  userId: number;

}