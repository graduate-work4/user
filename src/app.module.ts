import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [UserModule,TypeOrmModule.forRoot(
    {
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      database: 'test',
      username: 'postgres',
      password: '12345',
      entities: [__dirname + "/**/*.entity{.ts,.js}"],
      synchronize: true,
    }
  )],
  controllers: [],
  providers: [],
})
export class AppModule {}
